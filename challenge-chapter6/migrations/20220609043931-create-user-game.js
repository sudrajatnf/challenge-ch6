'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('User_games', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      username: {
        type: Sequelize.STRING,
        allowNull: false
      },
      password: {
        type: Sequelize.STRING,
        allowNull: false
      },
      createdAt: {
        // allowNull: false,
        type: Sequelize.DATE,
        default: CURRENT_TIMESTAMP
      },
      updatedAt: {
        // allowNull: false,
        type: Sequelize.DATE,
        default: CURRENT_TIMESTAMP
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('User_games');
  }
};